<html>
    <head>
        <meta charset="utf-8"/>
        <title> Register new user </title>    
    </head>
    <body>
        <?php
        session_start();
        $errors = array();
        $_SESSION['message'] = '';
        $mysqli = new mysqli('localhost', 'root', '12345', 'test');
        $count = 0;
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $mysqli->real_escape_string($_POST['username']);
            $firstname = $mysqli->real_escape_string($_POST['firstname']);
            $lastname = $mysqli->real_escape_string($_POST['lastname']);
            $password = $mysqli->real_escape_string($_POST['password']);
            $confirmpassword = $mysqli->real_escape_string($_POST['confirmpassword']);
            if ($password != $confirmpassword) {
                array_push($errors, "The two passwords do not match");
            }

            if (empty($username)) {
                array_push($errors, "Username is required");
                
            }
            if (empty($firstname)) {
                array_push($errors, "First name is required");
                
            }
            if (empty($lastname)) {
                array_push($errors, "Last name is required");
             
            }

            if ($password != $confirmpassword) {
                array_push($errors, "The two passwords do not match");
             
            }
            if (count($errors) == 0) {
                $password = md5($confirmpassword);
                $sql = "INSERT INTO tbl_user (username, firstname, lastname, password) "
                        . "VALUES ('$username', '$firstname', '$lastname', '$password') ";

                if ($mysqli->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $mysqli->error;
                  
                }
                $mysqli->close();
            } else {
                for ($x = 0; $x < count($errors); $x++) {
                echo ($errors[$x] . '<br>');
                }
            }

        }
        ?>
        <form action = "register_form.php" method = "post">
            Enter your first name: <br>
            <input name="firstname" type = "text">
            <br>
            Enter your last name: <br>
            <input name = 'lastname' type = 'text'>
            <br>
            Enter username: <br>
            <input name = 'username' type = 'text'>
            <br>
            Enter your password: <br>
            <input name = "password" type = 'password'>
            <br>
            Confirm password: <br>
            <input name = "confirmpassword" type = 'password'>
            <br><br>
            <input type="submit">

        </form>
    </body>
</html>